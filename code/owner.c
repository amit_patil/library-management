#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void owner_area(user_t *u)
{
    int choise,memberid;
    printf("\n\n=====WELCOME OWNER=====");
    do
    {
        printf ("\n0.SIGN OUT\n1. APPOINT LIBRARIAN\n2. EDIT PROFILE\n3. CHANGE PASSWORD\n4. FEES/FINE REPORT\n5. BOOK AVAILABILITY REPORT\n6. BOOK CATEGORIES / SUBJECTS REPORT\n7. DISPLAY ALL MEMBERS\n\nEnter Choise : ");
        scanf("%d",&choise);
        switch (choise)
        {
            case 1: // appoint librarian
                appoint_librarian();
                break;
            case 2: // edit profile
                    edit_profile_owner();
                break;
            case 3: //change_password();
                    change_password();
                break;
            case 4:// Payment History
					//printf("enter member id of the member : ");
					//scanf("%d",&memberid);
            		//payment_history(memberid);
					printf("\noption not available\n");
            		break;
            case 5: //book avaliablity check
                    //bookcopy_checkavail_details();
                    printf("\noption not available\n");
                break;
            case 6://BOOK CATEGORIES / SUBJECTS
                    printf("\noption not available\n");
                break;
			case 7://diplay all members 
					list_all_users();
        }
    } while (choise != 0);
    
}

void appoint_librarian()
{
    //input librarian details
    user_t u;
    user_accept(&u);
    //change user role to librarian
    strcpy(u.role, ROLE_LIBRARIAN);
    //add librarian into the user file
    user_add(&u);
}

void edit_profile_owner()  
{
	char email[30], password[20];
	int found =0;
	FILE *fp;
	user_t u;
	// intput user email  from user
	printf("\nenter email : ");
	scanf ("%s",email);
	printf("enter password : ");
	scanf ("%s",password);
	// open user file 
	fp = fopen(USER_DB, "rb+");
	if (fp == NULL)
	{
			perror("cannot open user file");
			exit(1);
	}
	//read user one by one and check if user with given email is found
	while(fread(&u, sizeof (user_t), 1, fp) > 0)
	{
		if (strcmp(email , u.email) == 0 && strcmp(password, u.password)==0)
		{
			found = 1;
			break;
		}
	}
	// if found
	if (found)
	{
		// input new user details from user
		long size = sizeof(user_t);
		user_t nu; 
		edit_user_details_owner(&nu);
		nu.id = u.id;
		strcpy(nu.email, u.email);
		strcpy(nu.password , u.password);
		strcpy(nu.role , u.role);
		// take file position one record behind 
		fseek(fp, -size, SEEK_CUR);
		// overwrite book details into the file 
		fwrite(&nu, size, 1, fp);
		printf("user profile updated \n");
	}
	else // if not found
		// show message to user that book not found
		printf("user not found\n");
	// close user file 
	fclose(fp);

}

void edit_user_details_owner(user_t *nu)
{
	printf("name: ");
	scanf("%s", nu->name);
	//printf("email: ");
	//scanf("%s", nu->email);
	printf("phone: ");
	scanf("%s", nu->phone);
	//printf("password: ");
	//scanf("%s", u->password);
}

// end 