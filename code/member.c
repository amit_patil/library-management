#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"
 
void member_area(user_t *u)
{
    int choise;
    char name[80];
    printf("\n=====WELCOME MEMBER=====");
    do
    {
        printf("\n0. SIGN OUT \n1. FIND BOOK\n2. EDIT PROFILE\n3. CHANGE PASSWORD\n4. BOOK AVAILABILITY\n. FIND BOOK\n\nEnter Choise : ");
        scanf("%d",&choise);
        switch(choise)
        {
            case 1: // find book 
                    printf ("enter book name: ");
                    scanf("%s, name");
                    book_find_by_name(name);
                break;
            case 2: // edit profile
                    edit_profile();
                break;
            case 3: //change password
                    change_password();
                break;
            case 4: // book availability
                    bookcopy_checkavail();
                break;
            case 5: //find book
					printf("\nenter book name : ");
					scanf("%s", name ); 
					book_find_by_name(name);
				break;
        }
    } while (choise != 0);
    
}

void bookcopy_checkavail()
{
    int book_id;
    FILE *fp;
    bookcopy_t bc;
    int count = 0;
    // input book id;
    printf("enter the book id : ");
    scanf("%d",&book_id);
    // open book copies file
    fp = fopen(BOOKCOPY_DB, "rb");
    if (fp == NULL)
    {
        perror("cannot open bookcopies file.");
        return;
    }

    // read bookcopy record on by one 
    while (fread(&bc, sizeof(bookcopy_t),1 ,fp) > 0)
    {
        // if book id is matching and the status is available,count the copies 
        if (bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL) == 0)
        {
            // bookcopy_display(&bc);
            count++;
        }
        
    }
    // close book copies file 
    fclose(fp);
    // print the message
    printf("number of copies availables : %d \n",count);
}

// end 